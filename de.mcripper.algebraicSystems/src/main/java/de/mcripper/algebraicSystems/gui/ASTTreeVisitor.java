package de.mcripper.algebraicSystems.gui;

import org.eclipse.swt.widgets.TreeItem;

import de.mcripper.algebraicSystems.ast.ASTNode;
import de.mcripper.algebraicSystems.ast.ASTNodeVisitor;
import de.mcripper.algebraicSystems.ast.AxiomDeclaration;
import de.mcripper.algebraicSystems.ast.AxiomsDeclarations;
import de.mcripper.algebraicSystems.ast.ConstantTerm;
import de.mcripper.algebraicSystems.ast.FormularDeclaration;
import de.mcripper.algebraicSystems.ast.FunctionCall;
import de.mcripper.algebraicSystems.ast.InfixTerm;
import de.mcripper.algebraicSystems.ast.Name;
import de.mcripper.algebraicSystems.ast.OperationDeclaration;
import de.mcripper.algebraicSystems.ast.OperationsDeclarations;
import de.mcripper.algebraicSystems.ast.PremiseDeclaration;
import de.mcripper.algebraicSystems.ast.ProductSortReference;
import de.mcripper.algebraicSystems.ast.SingleSortReference;
import de.mcripper.algebraicSystems.ast.SortDeclaration;
import de.mcripper.algebraicSystems.ast.SortsDeclarations;
import de.mcripper.algebraicSystems.ast.Subterms;
import de.mcripper.algebraicSystems.ast.SystemDeclaration;
import de.mcripper.algebraicSystems.ast.VariableDeclaration;
import de.mcripper.algebraicSystems.ast.VariableDeclarations;
import de.mcripper.algebraicSystems.ast.VariableNames;

public class ASTTreeVisitor implements ASTNodeVisitor {

    private final TreeItem treeItem;

    public ASTTreeVisitor(final TreeItem topItem) {
        this.treeItem = topItem;
    }

    public void handleSystemDeclaration(final SystemDeclaration systemDeclaration) {
        this.traverse(systemDeclaration.getSorts(), this.treeItem);
        this.traverse(systemDeclaration.getOperations(), this.treeItem);
        this.traverse(systemDeclaration.getAxioms(), this.treeItem);
    }

    private void traverse(final ASTNode node, final TreeItem parent) {
        if (node != null) {
            node.accept(new ASTTreeVisitor(parent));
        }
    }

    public void handleSortsDeclaration(final SortsDeclarations sortsDeclarations) {
        final TreeItem item = new TreeItem(this.treeItem, 0);
        item.setText("Sorts");
        item.setData(sortsDeclarations);
        for (final SortDeclaration decl : sortsDeclarations.getSorts()) {
            this.traverse(decl, item);
        }
    }

    public void handleSortDeclaration(final SortDeclaration sortDeclaration) {
        final TreeItem item = new TreeItem(this.treeItem, 0);
        item.setText("Sort");
        item.setData(sortDeclaration);
        this.traverse(sortDeclaration.getName(), item);
    }

    public void handleOperationsDeclarations(
            final OperationsDeclarations operationsDeclarations) {
        final TreeItem item = new TreeItem(this.treeItem, 0);
        item.setText("Operations");
        item.setData(operationsDeclarations);
        for (final OperationDeclaration op : operationsDeclarations.getOperations()) {
            this.traverse(op, item);
        }
    }

    public void handleOperationDeclaration(
            final OperationDeclaration operationDeclaration) {
        final TreeItem item = new TreeItem(this.treeItem, 0);
        item.setText("Operation");
        item.setData(operationDeclaration);
        this.traverse(operationDeclaration.getOperationName(), item);
        final TreeItem domain = new TreeItem(item, 0);
        domain.setText("Domain");
        final TreeItem codomain = new TreeItem(item, 0);
        codomain.setText("Codomain");
        this.traverse(operationDeclaration.getDomain(), domain);
        this.traverse(operationDeclaration.getCodomain(), codomain);
    }

    public void handleSingleSortReference(
            final SingleSortReference singleSortReference) {
        final TreeItem item = new TreeItem(this.treeItem, 0);
        item.setData(singleSortReference);
        item.setText(singleSortReference.getSortName().getValue());
    }

    public void handleName(final Name name) {
        final TreeItem item = new TreeItem(this.treeItem, 0);
        item.setText("Name: " + name.getValue());
        item.setData(name);
    }

    public void handleProductSortReference(
            final ProductSortReference productSortReference) {
        final TreeItem item = new TreeItem(this.treeItem, 0);
        item.setData(productSortReference);
        item.setText("Product");
        for (final SingleSortReference ref : productSortReference.getSortReferences()) {
            this.traverse(ref, item);
        }
    }

    public void handleAxiomsDeclarations(final AxiomsDeclarations axiomsDeclaration) {
        final TreeItem item = new TreeItem(this.treeItem, 0);
        item.setData(axiomsDeclaration);
        item.setText("Axioms");
        for (final AxiomDeclaration declaration : axiomsDeclaration.getAxioms()) {
            this.traverse(declaration, item);
        }
    }

    public void handleAxiomDeclaration(final AxiomDeclaration axiomDeclaration) {
        // TODO Auto-generated method stub

    }

    public void handleVariableNames(final VariableNames variableNames) {
        // TODO Auto-generated method stub

    }

    public void handleVariableDeclarartion(
            final VariableDeclaration variableDeclarartion) {
        // TODO Auto-generated method stub

    }

    public void handleVariableDeclarations(
            final VariableDeclarations variableDeclarations) {
        // TODO Auto-generated method stub

    }

    public void handleAxiomWithVariable(final AxiomDeclaration axiomWithVariable) {
        // TODO Auto-generated method stub

    }

    public void handleSubterms(
            final Subterms operationParameters) {
        // TODO Auto-generated method stub

    }

    public void handleConstantTerm(final ConstantTerm constantTerm) {
        // TODO Auto-generated method stub

    }

    public void handlePremiseDeclaration(final PremiseDeclaration premiseDeclaration) {
        // TODO Auto-generated method stub

    }

    public void handleFormularDeclaration(
            final FormularDeclaration formularDeclaration) {
        // TODO Auto-generated method stub

    }

    public void handleFunctionCall(final FunctionCall functionCall) {
        // TODO Auto-generated method stub

    }

    public void handleInfixTerm(final InfixTerm infixTerm) {
        // TODO Auto-generated method stub

    }


}
