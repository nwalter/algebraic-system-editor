package de.mcripper.algebraicSystems.gui;

import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.custom.ViewForm;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.wb.swt.SWTResourceManager;

import de.mcripper.algebraicSystems.ast.ASTNode;
import de.mcripper.algebraicSystems.parser.Parser;
import de.mcripper.algebraicSystems.parser.ParserException;
import de.mcripper.algebraicSystems.scanner.DefaultTokenVisitor;
import de.mcripper.algebraicSystems.scanner.Scanner;
import de.mcripper.algebraicSystems.scanner.TokenStream;
import de.mcripper.algebraicSystems.scanner.impl.ScannerException;
import de.mcripper.algebraicSystems.scanner.tokens.KeywordToken;
import de.mcripper.algebraicSystems.scanner.tokens.Token;

public class View extends ApplicationWindow {
    private StyledText styledText;
    private Tree astTree;

    /**
     * Create the application window.
     */
    public View() {
        super(null);
        this.createActions();
        this.addToolBar(SWT.FLAT | SWT.WRAP);
        this.addMenuBar();
        this.addStatusLine();
    }

    /**
     * Create contents of the application window.
     * @param parent
     */
    @Override
    protected Control createContents(final Composite parent) {
        final Composite container = new Composite(parent, SWT.NONE);
        container.setLayout(new FillLayout(SWT.HORIZONTAL));

        final SashForm sashForm = new SashForm(container, SWT.NONE);

        final ScrolledComposite scrolledComposite = new ScrolledComposite(sashForm, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
        scrolledComposite.setExpandHorizontal(true);
        scrolledComposite.setExpandVertical(true);


        this.styledText = new StyledText(scrolledComposite, SWT.BORDER);
        this.styledText.addModifyListener(new ModifyListener() {
            public void modifyText(final ModifyEvent e) {
                View.this.validateText();
            }
        });
        this.styledText.setFont(SWTResourceManager.getFont("Courier New", 9, SWT.NORMAL));
        scrolledComposite.setContent(this.styledText);
        scrolledComposite.setMinSize(this.styledText.computeSize(SWT.DEFAULT, SWT.DEFAULT));

        final ViewForm viewForm = new ViewForm(sashForm, SWT.NONE);

        final CTabFolder tabFolder_1 = new CTabFolder(viewForm, SWT.BORDER);
        viewForm.setContent(tabFolder_1);
        tabFolder_1.setSelectionBackground(Display.getCurrent().getSystemColor(SWT.COLOR_TITLE_INACTIVE_BACKGROUND_GRADIENT));

        final CTabItem tbtmAstView = new CTabItem(tabFolder_1, SWT.NONE);
        tbtmAstView.setImage(SWTResourceManager.getImage(View.class, "/org/eclipse/jface/dialogs/images/message_info.gif"));
        tbtmAstView.setText("AST View");
        tabFolder_1.setSelection(tbtmAstView);
        this.astTree = new Tree(tabFolder_1, SWT.BORDER);
        this.astTree.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(final SelectionEvent e) {
                if (e.item != null) {
                    final TreeItem selectedItem = (TreeItem) e.item;
                    View.this.handleItemSelected(selectedItem);
                }
            }
        });
        tbtmAstView.setControl(this.astTree);
        sashForm.setWeights(new int[] {310, 121});

        return container;
    }

    private void handleItemSelected(final TreeItem selectedItem) {
        final Object data = selectedItem.getData();
        if (data instanceof ASTNode) {
            final int start = ((ASTNode) data).getStart();
            final int end = ((ASTNode) data).getEnd();
            this.getTextArea().setSelection(start, end);
        }
    }

    private void validateText() {
        this.clearAst();
        this.getStatusLineManager().setErrorMessage("");
        final String text = this.getTextArea().getText();
        final Scanner scanner = new Scanner();
        try {
            final TokenStream tokens = scanner.scan(text);
            for (final Token token : tokens) {
                token.accept(new DefaultTokenVisitor() {
                    @Override
                    public void handleKeywordToken(final KeywordToken keywordToken) {
                        View.this.markKeywordText(keywordToken.getPosition(), keywordToken.getLength());
                    }
                    @Override
                    public void standardHandling(final Token token) {
                        View.this.removeStyle(token);
                    }

                });
            }

            final Parser parser = new Parser();
            final ASTNode  astNode = parser.parse(tokens);
            this.fillAst(astNode);
        } catch (final ScannerException e) {
            this.getStatusLineManager().setErrorMessage(e.getMessage());
        } catch (final ParserException e) {
            this.clearAst();
            this.getStatusLineManager().setErrorMessage(e.getMessage());
        }


    }

    private void clearAst() {
        this.getAstTree().removeAll();
    }

    private void fillAst(final ASTNode astNode) {
        final TreeItem topItem = new TreeItem(this.getAstTree(), 0);
        topItem.setText("AST");
        this.addNodes(topItem, astNode);
        this.expandAllItems(topItem);
    }

    private void addNodes(final TreeItem parent, final ASTNode node) {
        if (node == null) {
            return;
        }
        final TreeItem item = new TreeItem(parent, 0);
        final StringBuilder text = new StringBuilder(String.format("%S", node.getClass().getSimpleName()));
        if (node.getSubnodes().isEmpty()) {
            text.append(String.format(": %s", this.getText(node)));
        }
        item.setText(text.toString());
        item.setData(node);

        for (final ASTNode subnode : node.getSubnodes()) {
            this.addNodes(item, subnode);
        }
    }

    private String getText(final ASTNode node) {
        final String text = this.getTextArea().getText();
        return text.substring(node.getStart(), node.getEnd());
    }

    private void expandAllItems(final TreeItem parent) {
        parent.setExpanded(true);
        for (final TreeItem item : parent.getItems()) {
            this.expandAllItems(item);
        }
    }

    private void removeStyle(final Token token) {
        final StyleRange range = new StyleRange();
        range.start = token.getPosition();
        range.length = token.getLength();
        this.getTextArea().setStyleRange(range);
    }

    private void markKeywordText(final int position, final int length) {
        final StyleRange styleRange = new StyleRange();
        styleRange.start = position;
        styleRange.length = length;
        styleRange.fontStyle = SWT.BOLD;
        styleRange.underline = true;
        styleRange.foreground = Display.getCurrent().getSystemColor(SWT.COLOR_DARK_RED);

        this.getTextArea().setStyleRange(styleRange);
    }

    /**
     * Create the actions.
     */
    private void createActions() {
        // Create the actions
    }

    /**
     * Create the status line manager.
     * @return the status line manager
     */
    @Override
    protected StatusLineManager createStatusLineManager() {
        final StatusLineManager statusLineManager = new StatusLineManager();
        return statusLineManager;
    }

    /**
     * Launch the application.
     * @param args
     */
    public static void main(final String args[]) {
        try {
            final View window = new View();
            window.setBlockOnOpen(true);
            window.open();
            Display.getCurrent().dispose();
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Configure the shell.
     * @param newShell
     */
    @Override
    protected void configureShell(final Shell newShell) {
        super.configureShell(newShell);
        newShell.setText("Algebraic System Editor");
    }

    /**
     * Return the initial size of the window.
     */
    @Override
    protected Point getInitialSize() {
        return new Point(580, 428);
    }
    protected StyledText getTextArea() {
        return this.styledText;
    }

    public Tree getAstTree() {
        return this.astTree;
    }
}
