package de.mcripper.algebraicSystems.utils;

public class Condition {

    private boolean isTrue = false;

    public boolean isTrue() {
        return this.isTrue;
    }

    public void setTrue() {
        this.isTrue = true;
    }

}
