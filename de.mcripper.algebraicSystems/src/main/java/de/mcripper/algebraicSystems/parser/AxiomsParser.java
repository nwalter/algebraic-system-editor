package de.mcripper.algebraicSystems.parser;

import java.util.Iterator;

import com.google.common.base.Preconditions;

import de.mcripper.algebraicSystems.ast.ASTNode;
import de.mcripper.algebraicSystems.ast.AxiomDeclaration;
import de.mcripper.algebraicSystems.ast.AxiomsDeclarations;
import de.mcripper.algebraicSystems.ast.Name;
import de.mcripper.algebraicSystems.ast.SingleSortReference;
import de.mcripper.algebraicSystems.ast.SystemDeclaration;
import de.mcripper.algebraicSystems.ast.VariableDeclaration;
import de.mcripper.algebraicSystems.ast.VariableDeclarations;
import de.mcripper.algebraicSystems.ast.VariableNames;
import de.mcripper.algebraicSystems.scanner.Keyworder;
import de.mcripper.algebraicSystems.scanner.tokens.KeywordToken;
import de.mcripper.algebraicSystems.scanner.tokens.Symbol;
import de.mcripper.algebraicSystems.scanner.tokens.Token;
import de.mcripper.algebraicSystems.scanner.tokens.WordToken;

public class AxiomsParser extends SubParser {

    @Override
    public boolean canHandle(final Token token) {
        final KeywordToken keywordToken = this.asKeywordToken(token);
        return keywordToken != null && keywordToken.getKeyword().equals(Keyworder.AXIOMS);
    }

    @Override
    public void handle(final ParserTokenStream tokenStream, final Token currentToken,
            final ASTNode parent) throws ParserException {
        Preconditions.checkArgument(parent instanceof SystemDeclaration);

        final AxiomsDeclarations axioms = new AxiomsDeclarations(currentToken.getPosition(), -1);
        this.enterBlock(tokenStream);
        while (this.inBlock(tokenStream)) {
            this.parseAxiom(tokenStream, axioms);
        }
        axioms.setEnd(tokenStream.getPosition());
        final SystemDeclaration systemDeclaration = (SystemDeclaration) parent;
        systemDeclaration.setAxioms(axioms);
    }

    private void parseAxiom(final ParserTokenStream tokenStream, final AxiomsDeclarations axioms) {
        final AxiomDeclaration axiom = new AxiomDeclaration(tokenStream.getPosition(), -1);
        final VariableDeclarations vars = new VariableDeclarations(tokenStream.getPosition(), -1);
        axiom.setVariables(vars);
        final Iterator<Token> iteratorToDoubleColon = tokenStream.iteratorTo(Symbol.DOUBLE_COLON);
        while (iteratorToDoubleColon.hasNext()) {
            final VariableDeclaration declaration = new VariableDeclaration(tokenStream.getPosition(), -1);
            this.parseVariables(tokenStream, declaration);
            declaration.setEnd(tokenStream.getPosition());
            vars.addVariableDeclaration(declaration);
        }
        vars.setEnd(tokenStream.getPosition());
        this.parseFormular(axiom, tokenStream);
    }

    private void parseFormular(final AxiomDeclaration axiom, final ParserTokenStream token) {
    }

    private void parseVariables(final ParserTokenStream token,
            final VariableDeclaration vars) {
        token.stepBack();

        final Iterator<Token> iterator = token.iteratorTo(Symbol.COLON);
        final VariableNames names = new VariableNames(token.getPosition(), -1);
        while (iterator.hasNext()) {
            final Token current = iterator.next();
            if (current.isWord()) {
                names.addVariable(new Name(current));
            }
            token.skipIfNext(Symbol.COMMA);
        }
        names.setEnd(token.getPosition());
        vars.setVariables(names);
        token.skipNextSymbol(Symbol.COLON);
        final WordToken sort = token.getNext(WordToken.class);
        vars.setSort(new SingleSortReference(sort));
    }

}
