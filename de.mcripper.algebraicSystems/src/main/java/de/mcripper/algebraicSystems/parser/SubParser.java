package de.mcripper.algebraicSystems.parser;

import de.mcripper.algebraicSystems.ast.ASTNode;
import de.mcripper.algebraicSystems.scanner.tokens.KeywordToken;
import de.mcripper.algebraicSystems.scanner.tokens.Symbol;
import de.mcripper.algebraicSystems.scanner.tokens.SymbolToken;
import de.mcripper.algebraicSystems.scanner.tokens.Token;
import de.mcripper.algebraicSystems.scanner.tokens.WordToken;

public abstract class SubParser {

    public abstract boolean canHandle(Token token);

    public abstract void handle(ParserTokenStream token, Token currentToken, ASTNode parent) throws ParserException;

    public KeywordToken asKeywordToken(final Token token) {
        if (token instanceof KeywordToken) {
            return (KeywordToken) token;
        }
        return null;
    }

    protected SymbolToken asSymbolToken(final Token token) {
        if (token instanceof SymbolToken) {
            return (SymbolToken) token;
        }
        return null;
    }

    protected WordToken asWordToken(final Token token) {
        if (token instanceof WordToken) {
            return (WordToken) token;
        }
        return null;
    }

    protected void expect(final SymbolToken tokenStream, final Symbol symbol) {
        if (tokenStream.getSymbol() != symbol) {
            throw new ParserException(String.format("Expected %s but found %s",
                    symbol,
                    tokenStream.getSymbol()), tokenStream.getPosition());
        }
    }

    protected boolean inBlock(final ParserTokenStream tokenStream) {
        if (tokenStream.hasNext()) {
             final Token current = tokenStream.getNext();
             final SymbolToken symbolToken = this.asSymbolToken(current);
             tokenStream.stepBack();
             if (symbolToken != null && symbolToken.getSymbol() == Symbol.CURLY_CLOSE) {
                 tokenStream.skipNextSymbol(Symbol.CURLY_CLOSE);
                 return false;
             }
             return true;
        }
        return false;
    }

    protected void enterBlock(final ParserTokenStream tokenStream) {
        final SymbolToken symbolToken = tokenStream.getNext(SymbolToken.class);
        if (symbolToken.getSymbol() != Symbol.CURLY_OPEN){
            throw new ParserException(String.format("Excepted %s but found %s", Symbol.CURLY_OPEN, symbolToken.getSymbol()), tokenStream.getPosition());
        }
    }
}
