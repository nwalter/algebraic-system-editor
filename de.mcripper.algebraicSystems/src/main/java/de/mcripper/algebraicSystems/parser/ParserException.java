package de.mcripper.algebraicSystems.parser;

public class ParserException extends RuntimeException {

    private static final long serialVersionUID = 2897455516176624507L;
    private final int position;

    public ParserException(final String message, final int position) {
        super(message);
        this.position = position;
    }

    public int getPosition() {
        return this.position;
    }

}
