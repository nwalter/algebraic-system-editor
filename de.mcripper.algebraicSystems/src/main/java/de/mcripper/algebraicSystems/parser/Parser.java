package de.mcripper.algebraicSystems.parser;

import java.util.Iterator;
import java.util.List;

import com.google.common.collect.Lists;

import de.mcripper.algebraicSystems.ast.ASTNode;
import de.mcripper.algebraicSystems.ast.SystemDeclaration;
import de.mcripper.algebraicSystems.scanner.TokenStream;
import de.mcripper.algebraicSystems.scanner.tokens.EndToken;
import de.mcripper.algebraicSystems.scanner.tokens.Token;

public class Parser {

    private final List<SubParser> subParsers;

    public Parser() {
        this.subParsers = Lists.<SubParser>newArrayList(
                new SortsParser(),
                new OperationsParser(),
                new AxiomsParser()
                );
    }

    public ASTNode parse(final TokenStream tokenStream) throws ParserException {
        final ParserTokenStream parserTokenStream = new ParserTokenStream(tokenStream);
        final SystemDeclaration declaration = new SystemDeclaration(0, -1);
        final Iterator<Token> iterator = parserTokenStream.iterator();
        Token current = null;
        while (iterator.hasNext() && !((current = iterator.next()) instanceof EndToken)) {
            this.handleBySubparsers(current, parserTokenStream, declaration);
        }
        if (current == null) {
            return null;
        }
        declaration.setEnd(current.getPosition());
        return declaration;

    }

    private void handleBySubparsers(final Token current,
            final ParserTokenStream parserTokenStream, final ASTNode parent) throws ParserException {

        for (final SubParser subParser : this.subParsers) {
            if (subParser.canHandle(current)) {
                subParser.handle(parserTokenStream, current, parent);
            }
        }
    }
}
