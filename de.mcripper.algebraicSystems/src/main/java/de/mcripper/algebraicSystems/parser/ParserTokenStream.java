package de.mcripper.algebraicSystems.parser;

import java.util.Iterator;

import de.mcripper.algebraicSystems.scanner.TokenStream;
import de.mcripper.algebraicSystems.scanner.tokens.Symbol;
import de.mcripper.algebraicSystems.scanner.tokens.SymbolToken;
import de.mcripper.algebraicSystems.scanner.tokens.Token;

public class ParserTokenStream implements Iterable<Token> {
    private final TokenStream stream;
    private int position = 0;

    public ParserTokenStream(final TokenStream wrapped) {
        this.stream = wrapped;
    }

    public Token getNext() throws ParserException {
        if (this.position >= this.stream.size()) {
            throw new ParserException("Unexpected end of input", this.position);
        }
        return this.stream.getToken(this.position++);
    }

    public boolean hasNext() {
        return this.position < this.stream.size();
    }

    public Iterator<Token> iterator() {
        return new Iterator<Token>() {

            public boolean hasNext() {
                return ParserTokenStream.this.hasNext();
            }

            public Token next() {
                try {
                    return ParserTokenStream.this.getNext();
                } catch (final ParserException e) {
                    throw new RuntimeException(e);
                }
            }

            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }

    @SuppressWarnings("unchecked")
    public <T extends Token> T getNext(final Class<T> tokenClass) throws ParserException {
        final Token next = this.getNext();
        if (!tokenClass.isAssignableFrom(next.getClass())) {
            throw new ParserException("Unexpected token type", this.position);
        }
        return (T) next;
    }

    public int getPosition() {
        return this.position;
    }

    public void stepBack() {
        this.position--;
    }

    public void skipNextSymbol(final Symbol symbol) {
        final SymbolToken symbolToken = this.getNext(SymbolToken.class);
        if (symbolToken.getSymbol() != symbol) {
            throw new ParserException(String.format("Expected %s but found %s", symbol, symbolToken.getSymbol()), this.position);
        }
    }

    public Iterator<Token> iteratorTo(final Symbol symbol) {
        return new Iterator<Token>() {

            public boolean hasNext() {
                if (ParserTokenStream.this.hasNext()) {
                    final Token next = ParserTokenStream.this.getNext();
                    ParserTokenStream.this.stepBack();
                    return (!next.isSymbol() || ((SymbolToken) next).getSymbol() != symbol);
                }
                return false;
            }

            public Token next() {
                return ParserTokenStream.this.getNext();
            }

            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }

    public void skipIfNext(final Symbol symbol) {
        final SymbolToken symbolToken = this.getNext(SymbolToken.class);
        if (symbolToken.getSymbol() != symbol) {
            this.stepBack();
        }
    }
}
