package de.mcripper.algebraicSystems.parser;

import de.mcripper.algebraicSystems.ast.ASTNode;
import de.mcripper.algebraicSystems.ast.Name;
import de.mcripper.algebraicSystems.ast.SortDeclaration;
import de.mcripper.algebraicSystems.ast.SortsDeclarations;
import de.mcripper.algebraicSystems.ast.SystemDeclaration;
import de.mcripper.algebraicSystems.scanner.Keyworder;
import de.mcripper.algebraicSystems.scanner.tokens.KeywordToken;
import de.mcripper.algebraicSystems.scanner.tokens.Symbol;
import de.mcripper.algebraicSystems.scanner.tokens.Token;
import de.mcripper.algebraicSystems.scanner.tokens.WordToken;

public class SortsParser extends SubParser {

    @Override
    public boolean canHandle(final Token token) {
        final KeywordToken keywordToken = this.asKeywordToken(token);
        return keywordToken != null && keywordToken.getKeyword().equals(Keyworder.SORTS);
    }

    @Override
    public void handle(final ParserTokenStream tokenStream, final Token currentToken, final ASTNode parent) throws ParserException {
        assert parent instanceof SystemDeclaration;
        final SortsDeclarations sortsDeclarations = new SortsDeclarations(currentToken.getPosition(), -1);
        this.enterBlock(tokenStream);
        while (this.inBlock(tokenStream))  {
            final WordToken wordToken = tokenStream.getNext(WordToken.class);
            final SortDeclaration sortDeclaration = new SortDeclaration(wordToken);
            final Name name = new Name(wordToken);
            name.setValue(wordToken.getWord());
            sortDeclaration.setName(name);
            sortsDeclarations.addSortDeclaration(sortDeclaration);

            tokenStream.skipIfNext(Symbol.COMMA);
        }
        sortsDeclarations.setEnd(tokenStream.getPosition());
        final SystemDeclaration systemDeclaration = (SystemDeclaration) parent;
        systemDeclaration.setSorts(sortsDeclarations);

    }

}
