package de.mcripper.algebraicSystems.parser;

import de.mcripper.algebraicSystems.ast.ASTNode;
import de.mcripper.algebraicSystems.ast.Name;
import de.mcripper.algebraicSystems.ast.OperationDeclaration;
import de.mcripper.algebraicSystems.ast.OperationsDeclarations;
import de.mcripper.algebraicSystems.ast.ProductSortReference;
import de.mcripper.algebraicSystems.ast.SingleSortReference;
import de.mcripper.algebraicSystems.ast.SortsReferences;
import de.mcripper.algebraicSystems.ast.SystemDeclaration;
import de.mcripper.algebraicSystems.scanner.Keyworder;
import de.mcripper.algebraicSystems.scanner.tokens.KeywordToken;
import de.mcripper.algebraicSystems.scanner.tokens.Symbol;
import de.mcripper.algebraicSystems.scanner.tokens.SymbolToken;
import de.mcripper.algebraicSystems.scanner.tokens.Token;
import de.mcripper.algebraicSystems.scanner.tokens.WordToken;

public class OperationsParser extends SubParser {

    @Override
    public boolean canHandle(final Token token) {
        final KeywordToken keyword = this.asKeywordToken(token);
        return keyword != null && keyword.getKeyword().equals(Keyworder.OPERATIONS);
    }

    @Override
    public void handle(final ParserTokenStream tokenStream, final Token currentToken,
            final ASTNode parent) throws ParserException {
        final SystemDeclaration system = (SystemDeclaration) parent;
        final OperationsDeclarations opns = new OperationsDeclarations(currentToken.getPosition(), -1);
        this.enterBlock(tokenStream);
        while (this.inBlock(tokenStream)) {
            this.handleOperation(tokenStream, opns);
        }
        opns.setEnd(tokenStream.getPosition());
        system.setOperations(opns);
    }

    private void handleOperation(final ParserTokenStream token, final OperationsDeclarations opns) {
        final WordToken opName = token.getNext(WordToken.class);
        final OperationDeclaration declaration = new OperationDeclaration(opName.getPosition(), -1);
        final Name name = new Name(opName);
        name.setValue(opName.getWord());
        declaration.setOperationName(name);
        token.skipNextSymbol(Symbol.COLON);
        Token domainOrArrow = token.getNext();
        if (domainOrArrow.isWord()) {
            declaration.setDomain(this.createDomain((WordToken) domainOrArrow, token));
            domainOrArrow = token.getNext(SymbolToken.class);
        }
        this.expect((SymbolToken) domainOrArrow, Symbol.ARROW);

        Token semiOrCodomain = token.getNext();
        if (semiOrCodomain.isWord()) {
            declaration.setCodomain(this.createDomain((WordToken) semiOrCodomain, token));
            semiOrCodomain = token.getNext(SymbolToken.class);
        }
        this.expect((SymbolToken) semiOrCodomain, Symbol.SEMICOLON);
        declaration.setEnd(semiOrCodomain.getEnd());
        opns.addOperationDeclaration(declaration);
    }

    private SortsReferences createDomain(final WordToken domain, final ParserTokenStream token) {
        final SingleSortReference sortReference = this.createSingleSortRef(domain);
        SymbolToken commaOrArrow = token.getNext(SymbolToken.class);

        if (commaOrArrow.getSymbol() == Symbol.COMMA) {
            final ProductSortReference productReference = new ProductSortReference(sortReference.getStart(), -1);
            productReference.addReference(sortReference);
            while (commaOrArrow.getSymbol() == Symbol.COMMA) {
                final WordToken part = token.getNext(WordToken.class);
                productReference.addReference(this.createSingleSortRef(part));
                commaOrArrow = token.getNext(SymbolToken.class);
            }
            productReference.setEnd(commaOrArrow.getEnd());
            token.stepBack();
            return productReference;
        }

        token.stepBack();
        return sortReference;
    }

    private SingleSortReference createSingleSortRef(final WordToken domain) {
        final Name domainName = new Name(domain.getPosition(), domain.getEnd());
        domainName.setValue(domain.getWord());
        final SingleSortReference sortReference = new SingleSortReference(domain.getPosition(), domain.getEnd());
        sortReference.setSortName(domainName);
        return sortReference;
    }

}
