package de.mcripper.algebraicSystems.scanner;

import java.util.Set;

import com.google.common.collect.Sets;

import de.mcripper.algebraicSystems.scanner.tokens.KeywordToken;
import de.mcripper.algebraicSystems.scanner.tokens.Token;
import de.mcripper.algebraicSystems.scanner.tokens.WordToken;

public class Keyworder {

    public static final String AXIOMS = "axioms";
    public static final String OPERATIONS = "operations";
    public static final String SORTS = "sorts";
    private static Set<String> keywords;

    static {
        keywords = Sets.newHashSet(
                SORTS,
                OPERATIONS,
                AXIOMS);
    }


    public void markKeywords(final TokenStream tokenStream) {
        for (int i = 0; i < tokenStream.size(); i++) {
            final Token currentToken = tokenStream.getToken(i);
            currentToken.accept(new DefaultTokenVisitor(){

                @Override
                public void handleWordToken(final WordToken wordToken) {
                    final String word = wordToken.getWord();
                    if (keywords.contains(word)) {
                        tokenStream.replaceToken(wordToken, new KeywordToken(wordToken.getPosition(), word));
                    }
                }
            });
        }
    }
}
