package de.mcripper.algebraicSystems.scanner.tokens;


public abstract class Token {

    private final int position;

    public Token(final int position) {
        super();
        this.position = position;
    }

    public int getPosition() {
        return this.position;
    }

    public abstract int getLength();

    public boolean isWord() {
        return false;
    }

    public boolean isSymbol(){
        return false;
    }

    @Override
    public abstract String toString();

    public abstract void accept(TokenVisitor tokenVisitor);

    public int getEnd() {
        return this.position + this.getLength();
    }
}
