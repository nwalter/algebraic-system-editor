package de.mcripper.algebraicSystems.scanner.tokens;

public class EndToken extends Token {

    public EndToken(final int position) {
        super(position);
    }

    @Override
    public int getLength() {
        return 0;
    }

    @Override
    public String toString() {
        return String.format("[END %d]", this.getPosition());
    }

    @Override
    public void accept(final TokenVisitor visitor) {
        visitor.handleEndToken(this);
    }

}
