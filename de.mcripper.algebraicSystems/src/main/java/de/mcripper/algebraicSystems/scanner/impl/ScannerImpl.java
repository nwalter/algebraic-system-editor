package de.mcripper.algebraicSystems.scanner.impl;

import de.mcripper.algebraicSystems.scanner.TokenStream;
import de.mcripper.algebraicSystems.scanner.tokens.Symbol;

public class ScannerImpl {

    private final String input;
    private int position;
    private ScannerState state;

    public ScannerImpl(final String input) {
        this.input = input + Symbol.END.getSymbol();
        this.position = 0;
        this.state = new NeutralState(this);
    }

    public TokenStream scan() throws ScannerException {
        final TokenStream stream = new TokenStream();
        while (this.position < this.input.length()) {
            final char currentChar = this.input.charAt(this.position);
            this.state.scan(currentChar, stream);
        }

        return stream;
    }

    public void skip() {
        this.position++;
    }

    public int getPosition() {
        return this.position;
    }

    public void setState(final ScannerState newState) {
        this.state = newState;
    }

}
