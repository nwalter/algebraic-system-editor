package de.mcripper.algebraicSystems.scanner.impl;

public class ScannerException extends Exception {

    private static final long serialVersionUID = 1880613225935063447L;
    private final int position;

    public ScannerException(final String message, final int position) {
        super(message);
        this.position = position;
    }

    public int getPosition() {
        return this.position;
    }

}
