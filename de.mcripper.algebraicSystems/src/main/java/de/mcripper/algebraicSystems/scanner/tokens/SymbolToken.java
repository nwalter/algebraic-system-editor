package de.mcripper.algebraicSystems.scanner.tokens;

public class SymbolToken extends Token {

    private final Symbol symbol;

    public SymbolToken(final int position, final Symbol symbol) {
        super(position);
        this.symbol = symbol;
    }

    @Override
    public int getLength() {
        return this.symbol.getSymbol().length();
    }

    public Symbol getSymbol() {
        return this.symbol;
    }

    @Override
    public boolean isSymbol() {
        return true;
    }

    @Override
    public String toString() {
        return String.format("[%s, %d: %s]", this.symbol, this.getPosition(), this.getSymbol());
    }

    @Override
    public void accept(final TokenVisitor visitor) {
        visitor.handleSymbolToken(this);
    }

}
