package de.mcripper.algebraicSystems.scanner.tokens;

public class WordToken extends Token{

    private final String word;


    public WordToken(final int position, final String word) {
        super(position);
        this.word = word;
    }


    @Override
    public int getLength() {
        return this.word.length();
    }

    public String getWord() {
        return this.word;
    }

    @Override
    public boolean isWord() {
        return true;
    }

    @Override
    public String toString() {
        return String.format("[WORD, %d: %s]", this.getPosition(), this.word);
    }

    @Override
    public void accept(final TokenVisitor visitor) {
        visitor.handleWordToken(this);
    }

}
