package de.mcripper.algebraicSystems.scanner.impl;

import de.mcripper.algebraicSystems.scanner.TokenStream;
import de.mcripper.algebraicSystems.scanner.tokens.Symbol;

public class NeutralState extends ScannerState {

    public NeutralState(final ScannerImpl scanner) {
        super(scanner);
    }

    @Override
    public void scan(final char currentChar, final TokenStream stream) throws ScannerException {
        if (Character.isWhitespace(currentChar)) {
            this.skip();
            return;
        }
        if (Symbol.isSymbolChar(currentChar)) {
            this.newState(new SymbolState(this.getScanner()));
            return;
        }
        this.newState(new WordState(this.getScanner()));
    }

}
