package de.mcripper.algebraicSystems.scanner.impl;

import de.mcripper.algebraicSystems.scanner.TokenStream;
import de.mcripper.algebraicSystems.scanner.tokens.Symbol;
import de.mcripper.algebraicSystems.scanner.tokens.WordToken;

class WordState extends ScannerState {

    private final StringBuilder buffer;
    private final int startPosition;

    public WordState(final ScannerImpl scanner) {
        super(scanner);
        this.buffer = new StringBuilder();
        this.startPosition = this.getPosition();
    }

    @Override
    public void scan(final char currentChar, final TokenStream stream) {

        if (!Character.isWhitespace(currentChar) && !Symbol.isSymbolChar(currentChar)) {
            this.buffer.append(currentChar);
            this.skip();
            return;
        }

        stream.add(new WordToken(this.startPosition, this.buffer.toString()));
        this.newState(new NeutralState(this.getScanner()));
    }

}
