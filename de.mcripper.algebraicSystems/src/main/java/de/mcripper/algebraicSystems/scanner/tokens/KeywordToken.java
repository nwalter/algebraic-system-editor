package de.mcripper.algebraicSystems.scanner.tokens;

public class KeywordToken extends Token {

    private final String keyword;

    public KeywordToken(final int position, final String keyword) {
        super(position);
        this.keyword = keyword;
    }

    @Override
    public int getLength() {
        return this.keyword.length();
    }

    @Override
    public String toString() {
        return String.format("[KEYWORD, %d: %s]", this.getPosition(), this.keyword);
    }

    @Override
    public void accept(final TokenVisitor visitor) {
        visitor.handleKeywordToken(this);
    }

    public String getKeyword() {
        return this.keyword;
    }
}
