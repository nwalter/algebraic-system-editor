package de.mcripper.algebraicSystems.scanner;

import java.util.Iterator;
import java.util.List;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;

import de.mcripper.algebraicSystems.scanner.tokens.Token;

public class TokenStream implements Iterable<Token> {

    private final List<Token> tokens;

    public TokenStream() {
        super();
        this.tokens = Lists.newLinkedList();
    }

    public Token getToken(final int index) {
        return this.tokens.get(index);
    }

    public int size() {
        return this.tokens.size();
    }

    public void add(final Token token) {
        this.tokens.add(token);
    }

    @Override
    public String toString() {
        return Joiner.on(" ").join(this.tokens);
    }

    public Iterator<Token> iterator() {
        return this.tokens.iterator();
    }

    void replaceToken(final Token oldToken, final Token newToken) {
        final int index = this.tokens.indexOf(oldToken);
        this.tokens.set(index, newToken);
    }
}
