package de.mcripper.algebraicSystems.scanner.impl;

import java.util.EnumSet;
import java.util.Set;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;

import de.mcripper.algebraicSystems.scanner.TokenStream;
import de.mcripper.algebraicSystems.scanner.tokens.Symbol;
import de.mcripper.algebraicSystems.scanner.tokens.SymbolToken;

class SymbolState extends ScannerState {

    private Set<Symbol> potentialSymbols;
    private final StringBuilder bufferdSymbol;
    private final int startPosition;

    public SymbolState(final ScannerImpl scanner) {
        super(scanner);
        this.startPosition = this.getPosition();
        this.potentialSymbols = EnumSet.allOf(Symbol.class);
        this.bufferdSymbol = new StringBuilder();
    }

    @Override
    public void scan(final char currentChar, final TokenStream stream) throws ScannerException {
        this.bufferdSymbol.append(currentChar);
        final Set<Symbol> matchingSymbols = Sets.filter(this.potentialSymbols, new Predicate<Symbol>() {

            public boolean apply(final Symbol input) {
                final String symbol = input.getSymbol();
                return symbol.startsWith(SymbolState.this.bufferdSymbol.toString());
            }
        });
        if (matchingSymbols.isEmpty()) {
            final Set<Symbol> symbols = Sets.filter(this.potentialSymbols, new Predicate<Symbol>() {

                public boolean apply(final Symbol input) {
                    return SymbolState.this.bufferdSymbol.toString().startsWith(input.getSymbol());
                }
            });
            if (symbols.size() == 1) {
                stream.add(new SymbolToken(this.startPosition, Iterables.getOnlyElement(symbols)));
                this.newState(new NeutralState(this.getScanner()));
                return;
            }
            throw new ScannerException("Notdeterministic symbol sequence found!", this.getPosition());
        } else {
            this.potentialSymbols = EnumSet.copyOf(matchingSymbols);
            this.skip();
        }
    }

}
