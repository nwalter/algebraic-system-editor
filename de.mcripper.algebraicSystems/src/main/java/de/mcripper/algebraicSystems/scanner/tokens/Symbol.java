package de.mcripper.algebraicSystems.scanner.tokens;


public enum Symbol {
    CURLY_OPEN("{"),
    CURLY_CLOSE("}"),
    COLON(":"),
    DOUBLE_COLON("::"),
    ARROW("->"),
    BRACKET_OPEN("("),
    BRACKET_CLOSE(")"),
    COMMA(","),
    SEMICOLON(";"),
    IMPLIES("=>"),
    EQUALS("="),
    END("$")
    ;

    private final String symbol;

    private Symbol(final String symbol) {
        this.symbol = symbol;

    }

    public String getSymbol() {
        return this.symbol;
    }

    public static boolean isSymbolChar(final char currentChar) {
        for (final Symbol symbol : Symbol.values()) {
            if (symbol.getSymbol().charAt(0) == currentChar) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return this.symbol;
    }
}
