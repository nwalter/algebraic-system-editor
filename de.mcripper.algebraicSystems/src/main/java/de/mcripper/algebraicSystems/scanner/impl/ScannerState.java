package de.mcripper.algebraicSystems.scanner.impl;

import de.mcripper.algebraicSystems.scanner.TokenStream;


abstract class ScannerState {
    private final ScannerImpl scanner;

    public ScannerState(final ScannerImpl scanner) {
        super();
        this.scanner = scanner;
    }

    protected void newState(final ScannerState newState) {
        this.scanner.setState(newState);
    }

    protected int getPosition() {
        return this.scanner.getPosition();
    }

    protected void skip() {
        this.scanner.skip();
    }

    protected ScannerImpl getScanner() {
        return this.scanner;
    }

    public abstract void scan(char currentChar, TokenStream stream) throws ScannerException;

}
