package de.mcripper.algebraicSystems.scanner;

import de.mcripper.algebraicSystems.scanner.impl.ScannerException;
import de.mcripper.algebraicSystems.scanner.impl.ScannerImpl;

public class Scanner {

    public TokenStream scan(final String input) throws ScannerException {
        final ScannerImpl impl = new ScannerImpl(input);
        final TokenStream tokens = impl.scan();
        final Keyworder keyworder = new Keyworder();
        keyworder.markKeywords(tokens);
        return tokens;
    }

}
