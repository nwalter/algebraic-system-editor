package de.mcripper.algebraicSystems.scanner.tokens;

public interface TokenVisitor {

    void handleSymbolToken(SymbolToken symbolToken);

    void handleWordToken(WordToken wordToken);

    void handleKeywordToken(KeywordToken keywordToken);

    void handleEndToken(EndToken endToken);

}
