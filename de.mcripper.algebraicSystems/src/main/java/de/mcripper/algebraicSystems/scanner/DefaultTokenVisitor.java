package de.mcripper.algebraicSystems.scanner;

import de.mcripper.algebraicSystems.scanner.tokens.EndToken;
import de.mcripper.algebraicSystems.scanner.tokens.KeywordToken;
import de.mcripper.algebraicSystems.scanner.tokens.SymbolToken;
import de.mcripper.algebraicSystems.scanner.tokens.Token;
import de.mcripper.algebraicSystems.scanner.tokens.TokenVisitor;
import de.mcripper.algebraicSystems.scanner.tokens.WordToken;

public abstract class DefaultTokenVisitor implements TokenVisitor {

    public void handleSymbolToken(final SymbolToken symbolToken) {
        this.standardHandling(symbolToken);
    }

    public void handleWordToken(final WordToken wordToken) {
        this.standardHandling(wordToken);
    }

    public void handleKeywordToken(final KeywordToken keywordToken) {
        this.standardHandling(keywordToken);
    }

    public void handleEndToken(final EndToken endToken) {
        this.standardHandling(endToken);
    }

    public void standardHandling(final Token token) {
    }

}
