package de.mcripper.algebraicSystems.model;

public class Sort {

    private final String name;

    public Sort(final String name) {
        super();
        this.name = name;
    }

    public String getName() {
        return this.name;
    }


}
