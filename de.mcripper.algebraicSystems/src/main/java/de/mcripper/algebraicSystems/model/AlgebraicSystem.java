package de.mcripper.algebraicSystems.model;

import java.util.List;

import com.google.common.collect.Lists;


public class AlgebraicSystem {
    private final List<Sort> sorts;

    public AlgebraicSystem() {
        super();
        this.sorts = Lists.newArrayList();
    }

    public void addSort(final Sort sort) {
        this.sorts.add(sort);
    }
}
