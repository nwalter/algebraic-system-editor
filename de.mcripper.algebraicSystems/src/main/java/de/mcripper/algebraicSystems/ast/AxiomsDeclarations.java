package de.mcripper.algebraicSystems.ast;

import java.util.Collection;
import java.util.List;

import com.google.common.collect.Lists;

public class AxiomsDeclarations extends ASTNode {

    private final List<AxiomDeclaration> axioms;

    public AxiomsDeclarations(final int start, final int end) {
        super(start, end);
        this.axioms = Lists.newArrayList();
    }

    @Override
    public void accept(final ASTNodeVisitor visitor) {
        visitor.handleAxiomsDeclarations(this);
    }

    public List<AxiomDeclaration> getAxioms() {
        return this.axioms;
    }

    @Override
    public Collection<? extends ASTNode> getSubnodes() {
        return this.axioms;
    }

}
