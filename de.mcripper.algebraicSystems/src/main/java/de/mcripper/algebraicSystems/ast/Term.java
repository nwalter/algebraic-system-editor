package de.mcripper.algebraicSystems.ast;

import de.mcripper.algebraicSystems.scanner.tokens.Token;

public abstract class Term extends ASTNode {

    public Term(final int start, final int end) {
        super(start, end);
    }

    public Term(final Token token) {
        super(token);
    }

}
