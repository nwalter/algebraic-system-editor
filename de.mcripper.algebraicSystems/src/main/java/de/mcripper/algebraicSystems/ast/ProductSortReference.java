package de.mcripper.algebraicSystems.ast;

import java.util.Collection;
import java.util.List;

import com.google.common.collect.Lists;

public class ProductSortReference extends SortsReferences {

    private final List<SingleSortReference> sorts;

    public ProductSortReference(final int start, final int end) {
        super(start, end);
        this.sorts = Lists.newArrayList();
    }

    public void addReference(final SingleSortReference reference) {
        this.sorts.add(reference);
    }

    @Override
    public void accept(final ASTNodeVisitor visitor) {
        visitor.handleProductSortReference(this);
    }

    public List<SingleSortReference> getSortReferences() {
        return this.sorts;
    }

    @Override
    public Collection<? extends ASTNode> getSubnodes() {
        return this.sorts;
    }

}
