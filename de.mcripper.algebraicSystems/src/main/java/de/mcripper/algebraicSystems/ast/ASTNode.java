package de.mcripper.algebraicSystems.ast;

import java.util.Collection;

import de.mcripper.algebraicSystems.scanner.tokens.Token;

public abstract class ASTNode {
    private final int start;
    private int end;

    public ASTNode(final int start, final int end) {
        super();
        this.start = start;
        this.end = end;
    }

    public ASTNode(final Token token) {
        this(token.getPosition(), token.getEnd());
    }

    public int getStart() {
        return this.start;
    }

    public int getEnd() {
        return this.end;
    }

    public void setEnd(final int end) {
        this.end = end;
    }

    public abstract void accept(ASTNodeVisitor visitor);

    public abstract Collection<? extends ASTNode> getSubnodes();

}
