package de.mcripper.algebraicSystems.ast;

import java.util.Collection;

import com.google.common.collect.Lists;


public class VariableDeclaration extends ASTNode {

    private SingleSortReference sort;
    private VariableNames variables;

    public VariableDeclaration(final int start, final int end) {
        super(start, end);
    }

    public VariableNames getVariables() {
        return this.variables;
    }

    public void setVariables(final VariableNames variables) {
        this.variables = variables;
    }

    public void setSort(final SingleSortReference sort) {
        this.sort = sort;
    }

    public SingleSortReference getSort() {
        return this.sort;
    }

    @Override
    public void accept(final ASTNodeVisitor visitor) {
        visitor.handleVariableDeclarartion(this);
    }

    @Override
    public Collection<? extends ASTNode> getSubnodes() {
        return Lists.newArrayList(this.variables, this.sort);
    }

}
