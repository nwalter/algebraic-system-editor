package de.mcripper.algebraicSystems.ast;

import de.mcripper.algebraicSystems.scanner.tokens.Token;

public abstract class AtomicTerm extends Term {

    public AtomicTerm(final int start, final int end) {
        super(start, end);
    }

    public AtomicTerm(final Token token) {
        super(token);
    }

}
