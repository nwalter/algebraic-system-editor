package de.mcripper.algebraicSystems.ast;

import java.util.Collection;
import java.util.Collections;

import de.mcripper.algebraicSystems.scanner.tokens.Token;
import de.mcripper.algebraicSystems.scanner.tokens.WordToken;

public class Name extends ASTNode {

    private String value;

    public Name(final int start, final int end) {
        super(start, end);
    }

    public Name(final Token token) {
        super(token);
        if (token instanceof WordToken) {
            this.setValue(((WordToken) token).getWord());
        }
    }


    @Override
    public void accept(final ASTNodeVisitor visitor) {
        visitor.handleName(this);
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(final String value) {
        this.value = value;
    }

    @Override
    public Collection<? extends ASTNode> getSubnodes() {
        return Collections.emptyList();
    }

}
