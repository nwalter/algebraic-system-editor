package de.mcripper.algebraicSystems.ast;

import java.util.Collection;

import com.google.common.collect.Lists;


public class InfixTerm extends Term {

    private AtomicTerm left;
    private AtomicTerm rigth;

    public InfixTerm(final int start, final int end) {
        super(start, end);
    }

    public AtomicTerm getLeft() {
        return this.left;
    }

    public void setLeft(final AtomicTerm left) {
        this.left = left;
    }

    public AtomicTerm getRigth() {
        return this.rigth;
    }

    public void setRigth(final AtomicTerm rigth) {
        this.rigth = rigth;
    }

    @Override
    public void accept(final ASTNodeVisitor visitor) {
        visitor.handleInfixTerm(this);
    }

    @Override
    public Collection<? extends ASTNode> getSubnodes() {
        return Lists.newArrayList(
                this.left,
                this.rigth);
    }

}
