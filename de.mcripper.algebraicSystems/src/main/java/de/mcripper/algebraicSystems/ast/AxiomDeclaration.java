package de.mcripper.algebraicSystems.ast;

import java.util.Collection;

import com.google.common.collect.Lists;

public class AxiomDeclaration extends ASTNode {

    private FormularDeclaration formular;
    private VariableDeclarations variables;

    public AxiomDeclaration(final int start, final int end) {
        super(start, end);
    }

    public FormularDeclaration getFormular() {
        return this.formular;
    }

    public void setFormular(final FormularDeclaration formular) {
        this.formular = formular;
    }

    public VariableDeclarations getVariables() {
        return this.variables;
    }

    public void setVariables(final VariableDeclarations variables) {
        this.variables = variables;
    }

    @Override
    public void accept(final ASTNodeVisitor visitor) {
        visitor.handleAxiomDeclaration(this);
    }

    @Override
    public Collection<? extends ASTNode> getSubnodes() {
        return Lists.newArrayList(
                this.variables,
                this.formular);
    }

}
