package de.mcripper.algebraicSystems.ast;

import java.util.Collection;

import com.google.common.collect.Lists;

public final class FormularDeclaration extends ASTNode {

    private PremiseDeclaration premise;
    private Term conclusion;

    public FormularDeclaration(final int start, final int end) {
        super(start, end);
    }

    public PremiseDeclaration getPremise() {
        return this.premise;
    }

    public void setPremise(final PremiseDeclaration premise) {
        this.premise = premise;
    }

    public Term getConclusion() {
        return this.conclusion;
    }

    public void setConclusion(final Term conclusion) {
        this.conclusion = conclusion;
    }

    @Override
    public void accept(final ASTNodeVisitor visitor) {
        visitor.handleFormularDeclaration(this);
    }

    @Override
    public Collection<? extends ASTNode> getSubnodes() {
        return Lists.newArrayList(
                this.premise,
                this.conclusion
                );
    }

}
