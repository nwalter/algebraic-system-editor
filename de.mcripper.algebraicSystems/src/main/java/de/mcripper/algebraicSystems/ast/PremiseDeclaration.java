package de.mcripper.algebraicSystems.ast;

import java.util.Collection;
import java.util.List;

import com.google.common.collect.Lists;

public class PremiseDeclaration extends ASTNode {

    private final List<Term> conditions;

    public PremiseDeclaration(final int start, final int end) {
        super(start, end);
        this.conditions = Lists.newArrayList();
    }

    public void addCondition(final Term value) {
        this.conditions.add(value);
    }

    public List<Term> getConditions() {
        return this.conditions;
    }

    @Override
    public void accept(final ASTNodeVisitor visitor) {
        visitor.handlePremiseDeclaration(this);
    }

    @Override
    public Collection<? extends ASTNode> getSubnodes() {
        return this.conditions;
    }
}
