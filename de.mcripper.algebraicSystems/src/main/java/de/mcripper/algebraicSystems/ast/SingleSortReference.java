package de.mcripper.algebraicSystems.ast;

import java.util.Collection;
import java.util.Collections;

import de.mcripper.algebraicSystems.scanner.tokens.Token;

public class SingleSortReference extends SortsReferences {

    public SingleSortReference(final int start, final int end) {
        super(start, end);
    }


    public SingleSortReference(final Token token) {
        super(token);
    }

    private Name sortName;

    @Override
    public void accept(final ASTNodeVisitor visitor) {
        visitor.handleSingleSortReference(this);
    }

    public void setSortName(final Name name) {
        this.sortName = name;
    }

    public Name getSortName() {
        return this.sortName;
    }

    @Override
    public Collection<? extends ASTNode> getSubnodes() {
        return Collections.singletonList(this.sortName);
    }

}
