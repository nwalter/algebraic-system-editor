package de.mcripper.algebraicSystems.ast;

import java.util.Collection;
import java.util.List;

import com.google.common.collect.Lists;

public class VariableDeclarations extends ASTNode {

    private final List<VariableDeclaration> variables;

    public VariableDeclarations(final int start, final int end) {
        super(start, end);
        this.variables = Lists.newArrayList();
    }

    public List<VariableDeclaration> getVariables() {
        return this.variables;
    }

    public void addVariableDeclaration(final VariableDeclaration value) {
        this.variables.add(value);
    }

    @Override
    public void accept(final ASTNodeVisitor visitor) {
        visitor.handleVariableDeclarations(this);
    }

    @Override
    public Collection<? extends ASTNode> getSubnodes() {
        return this.variables;
    }

}
