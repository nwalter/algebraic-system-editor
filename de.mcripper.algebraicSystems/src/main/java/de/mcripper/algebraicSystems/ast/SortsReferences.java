package de.mcripper.algebraicSystems.ast;

import de.mcripper.algebraicSystems.scanner.tokens.Token;


public abstract class SortsReferences extends ASTNode {

    public SortsReferences(final int start, final int end) {
        super(start, end);
    }

    public SortsReferences(final Token token) {
        super(token);
    }

}
