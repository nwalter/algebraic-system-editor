package de.mcripper.algebraicSystems.ast;

import java.util.Collection;

import com.google.common.collect.Lists;

public class SystemDeclaration extends ASTNode {

    private SortsDeclarations sorts;
    private OperationsDeclarations operations;
    private AxiomsDeclarations axioms;

    public SystemDeclaration(final int start, final int end) {
        super(start, end);
    }

    @Override
    public void accept(final ASTNodeVisitor visitor) {
        visitor.handleSystemDeclaration(this);
    }

    public SortsDeclarations getSorts() {
        return this.sorts;
    }

    public void setSorts(final SortsDeclarations sorts) {
        this.sorts = sorts;
    }

    public void setOperations(final OperationsDeclarations operations) {
        this.operations = operations;
    }

    public OperationsDeclarations getOperations() {
        return this.operations;
    }

    public void setAxioms(final AxiomsDeclarations axioms) {
        this.axioms = axioms;
    }

    public AxiomsDeclarations getAxioms() {
        return this.axioms;
    }

    @Override
    public Collection<? extends ASTNode> getSubnodes() {
        return Lists.newArrayList(
                this.sorts,
                this.operations,
                this.axioms);
    }
}
