package de.mcripper.algebraicSystems.ast;

import java.util.Collection;

import com.google.common.collect.Lists;

public class FunctionCall extends AtomicTerm {

    private Name name;
    private Subterms subterms;

    public FunctionCall(final int start, final int end) {
        super(start, end);
    }

    public Name getName() {
        return this.name;
    }

    public void setName(final Name name) {
        this.name = name;
    }

    public Subterms getSubterms() {
        return this.subterms;
    }

    public void setSubterms(final Subterms subterms) {
        this.subterms = subterms;
    }

    @Override
    public void accept(final ASTNodeVisitor visitor) {
        visitor.handleFunctionCall(this);
    }

    @Override
    public Collection<? extends ASTNode> getSubnodes() {
        return Lists.newArrayList(
                this.name,
                this.subterms);
    }

}
