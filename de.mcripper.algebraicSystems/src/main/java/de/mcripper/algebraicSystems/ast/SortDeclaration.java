package de.mcripper.algebraicSystems.ast;

import java.util.Collection;
import java.util.Collections;

import de.mcripper.algebraicSystems.scanner.tokens.Token;

public class SortDeclaration extends ASTNode {

    private Name name;

    public SortDeclaration(final int start, final int end) {
        super(start, end);
    }

    public SortDeclaration(final Token token) {
        super(token);
    }

    @Override
    public void accept(final ASTNodeVisitor visitor) {
        visitor.handleSortDeclaration(this);
    }

    public void setName(final Name name2) {
        this.name = name2;
    }

    public Name getName() {
        return this.name;
    }

    @Override
    public Collection<? extends ASTNode> getSubnodes() {
        return Collections.singleton(this.name);
    }

}
