package de.mcripper.algebraicSystems.ast;

import java.util.Collection;
import java.util.List;

import com.google.common.collect.Lists;

public class Subterms extends ASTNode {

    private final List<Term> subterms;

    public Subterms(final int start, final int end) {
        super(start, end);
        this.subterms = Lists.newArrayList();
    }

    public void addSubterm(final Term value) {
        this.subterms.add(value);
    }

    public List<Term> getSubterms() {
        return this.subterms;
    }

    @Override
    public void accept(final ASTNodeVisitor visitor) {
        visitor.handleSubterms(this);
    }

    @Override
    public Collection<? extends ASTNode> getSubnodes() {
        return this.subterms;
    }

}
