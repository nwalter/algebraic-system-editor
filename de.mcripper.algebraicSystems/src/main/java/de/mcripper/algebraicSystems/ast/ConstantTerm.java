package de.mcripper.algebraicSystems.ast;

import java.util.Collection;
import java.util.Collections;

import de.mcripper.algebraicSystems.scanner.tokens.Token;

public class ConstantTerm extends AtomicTerm {

    private Name name;

    public ConstantTerm(final Token token) {
        super(token);
    }

    public Name getName() {
        return this.name;
    }

    public void setName(final Name name) {
        this.name = name;
    }

    @Override
    public void accept(final ASTNodeVisitor visitor) {
        visitor.handleConstantTerm(this);
    }

    @Override
    public Collection<? extends ASTNode> getSubnodes() {
        return Collections.singleton(this.name);
    }

}
