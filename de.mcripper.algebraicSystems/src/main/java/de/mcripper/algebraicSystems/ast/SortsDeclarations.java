package de.mcripper.algebraicSystems.ast;

import java.util.Collection;
import java.util.List;

import com.google.common.collect.Lists;

public class SortsDeclarations extends ASTNode {

    private final List<SortDeclaration> sorts;

    public SortsDeclarations(final int start, final int end) {
        super(start, end);
        this.sorts = Lists.newArrayList();
    }

    @Override
    public void accept(final ASTNodeVisitor visitor) {
        visitor.handleSortsDeclaration(this);
    }

    public List<SortDeclaration> getSorts() {
        return this.sorts;
    }

    public void addSortDeclaration(final SortDeclaration sort) {
        this.sorts.add(sort);
    }

    @Override
    public Collection<? extends ASTNode> getSubnodes() {
        return this.sorts;
    }

}
