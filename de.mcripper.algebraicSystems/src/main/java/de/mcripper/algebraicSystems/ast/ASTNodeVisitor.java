package de.mcripper.algebraicSystems.ast;

public interface ASTNodeVisitor {

    void handleSystemDeclaration(SystemDeclaration systemDeclaration);

    void handleSortsDeclaration(SortsDeclarations sortsDeclarations);

    void handleSortDeclaration(SortDeclaration sortDeclaration);

    void handleOperationsDeclarations(
            OperationsDeclarations operationsDeclarations);

    void handleOperationDeclaration(OperationDeclaration operationDeclaration);

    void handleSingleSortReference(SingleSortReference singleSortReference);

    void handleName(Name name);

    void handleProductSortReference(ProductSortReference productSortReference);

    void handleAxiomsDeclarations(AxiomsDeclarations axiomsDeclaration);

    void handleAxiomDeclaration(AxiomDeclaration axiomDeclaration);

    void handleVariableNames(VariableNames variableNames);

    void handleVariableDeclarartion(VariableDeclaration variableDeclarartion);

    void handleVariableDeclarations(VariableDeclarations variableDeclarations);

    void handleAxiomWithVariable(AxiomDeclaration axiomWithVariable);

    void handleSubterms(Subterms operationParameters);

    void handleConstantTerm(ConstantTerm constantTerm);

    void handlePremiseDeclaration(PremiseDeclaration premiseDeclaration);

    void handleFormularDeclaration(FormularDeclaration formularDeclaration);

    void handleFunctionCall(FunctionCall functionCall);

    void handleInfixTerm(InfixTerm infixTerm);

}
