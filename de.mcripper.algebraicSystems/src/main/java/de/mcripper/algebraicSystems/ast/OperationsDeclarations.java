package de.mcripper.algebraicSystems.ast;

import java.util.Collection;
import java.util.List;

import com.google.common.collect.Lists;

public class OperationsDeclarations extends ASTNode {

    private final List<OperationDeclaration> operations;

    public OperationsDeclarations(final int start, final int end) {
        super(start, end);
        this.operations = Lists.newArrayList();
    }

    @Override
    public void accept(final ASTNodeVisitor visitor) {
        visitor.handleOperationsDeclarations(this);
    }

    public List<OperationDeclaration> getOperations() {
        return this.operations;
    }

    public void addOperationDeclaration(final OperationDeclaration operation) {
        this.operations.add(operation);
    }

    @Override
    public Collection<? extends ASTNode> getSubnodes() {
        return this.operations;
    }

}
