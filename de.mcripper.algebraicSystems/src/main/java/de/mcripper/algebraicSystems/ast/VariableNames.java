package de.mcripper.algebraicSystems.ast;

import java.util.Collection;
import java.util.List;

import com.google.common.collect.Lists;

public class VariableNames extends ASTNode {

    private final List<Name> names;

    public VariableNames(final int start, final int end) {
        super(start, end);
        this.names = Lists.newArrayList();
    }

    public List<Name> getNames() {
        return this.names;
    }

    public void addVariable(final Name value) {
        this.names.add(value);
    }

    @Override
    public void accept(final ASTNodeVisitor visitor) {
        visitor.handleVariableNames(this);
    }

    @Override
    public Collection<? extends ASTNode> getSubnodes() {
        return this.names;
    }

}
