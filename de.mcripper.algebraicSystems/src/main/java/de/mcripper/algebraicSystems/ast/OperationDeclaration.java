package de.mcripper.algebraicSystems.ast;

import java.util.Collection;

import com.google.common.collect.Lists;


public class OperationDeclaration extends ASTNode {

    private Name operationName;
    private SortsReferences domain;
    private SortsReferences codomain;

    public OperationDeclaration(final int start, final int end) {
        super(start, end);
    }

    @Override
    public void accept(final ASTNodeVisitor visitor) {
        visitor.handleOperationDeclaration(this);
    }

    public Name getOperationName() {
        return this.operationName;
    }

    public void setOperationName(final Name operationName) {
        this.operationName = operationName;
    }

    public SortsReferences getDomain() {
        return this.domain;
    }

    public void setDomain(final SortsReferences domain) {
        this.domain = domain;
    }

    public SortsReferences getCodomain() {
        return this.codomain;
    }

    public void setCodomain(final SortsReferences codomain) {
        this.codomain = codomain;
    }

    @Override
    public Collection<? extends ASTNode> getSubnodes() {
        return Lists.newArrayList(
                this.operationName,
                this.domain,
                this.codomain);
    }

}
