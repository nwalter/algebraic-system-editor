package de.mcripper.algebraicSystems.scanner;

import static org.junit.Assert.assertThat;

import org.hamcrest.core.Is;
import org.junit.Test;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;

import de.mcripper.algebraicSystems.scanner.tokens.KeywordToken;
import de.mcripper.algebraicSystems.scanner.tokens.SymbolToken;
import de.mcripper.algebraicSystems.scanner.tokens.WordToken;

public class ScannerTest extends Scanner {

    @Test
    public void testSorts() throws Exception {
        final String input = "sorts {\n"
                + "    N,Q\n"
                + "}";
        final Scanner scanner = new Scanner();
        final TokenStream tokens = scanner.scan(input);

        assertThat(tokens.size(), Is.is(6));
        assertThat((KeywordToken)tokens.getToken(0), Is.isA(KeywordToken.class));
        assertThat((SymbolToken)tokens.getToken(1), Is.isA(SymbolToken.class));
        assertThat((WordToken)tokens.getToken(2), Is.isA(WordToken.class));
        assertThat((SymbolToken)tokens.getToken(3), Is.isA(SymbolToken.class));
        assertThat((WordToken)tokens.getToken(4), Is.isA(WordToken.class));
        assertThat((SymbolToken)tokens.getToken(5), Is.isA(SymbolToken.class));
    }

    @Test
    public void testCompleteSystem() throws Exception {
        final String input = Resources.toString(ScannerTest.class.getResource("system.alg"), Charsets.UTF_8);
        final Scanner scanner = new Scanner();
        final TokenStream tokens = scanner.scan(input);

        assertThat(tokens.size(), Is.is(58));

    }

}
