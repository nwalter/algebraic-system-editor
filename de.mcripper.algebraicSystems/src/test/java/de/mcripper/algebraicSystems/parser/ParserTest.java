package de.mcripper.algebraicSystems.parser;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.util.Iterator;
import java.util.List;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.core.Is;
import org.junit.Test;

import com.google.common.collect.Lists;

import de.mcripper.algebraicSystems.ast.OperationDeclaration;
import de.mcripper.algebraicSystems.ast.ProductSortReference;
import de.mcripper.algebraicSystems.ast.SingleSortReference;
import de.mcripper.algebraicSystems.ast.SortsReferences;
import de.mcripper.algebraicSystems.ast.SystemDeclaration;
import de.mcripper.algebraicSystems.scanner.Scanner;
import de.mcripper.algebraicSystems.scanner.impl.ScannerException;

public class ParserTest {

    @Test
    public void testSingleSort() throws Exception {
        final String input = "sorts { N }";
        final SystemDeclaration system = parse(input);
        assertNotNull(system);
        assertNotNull(system.getSorts());
        assertThat(system.getSorts().getSorts().size(), Is.is(1));
        assertThat(system.getSorts().getSorts().get(0).getName().getValue(), Is.is("N"));
    }

    @Test
    public void testTwoSort() throws Exception {
        final String input = "sorts { N,Q }";
        final SystemDeclaration system = parse(input);
        assertNotNull(system);
        assertNotNull(system.getSorts());
        assertThat(system.getSorts().getSorts().size(), Is.is(2));
        assertThat(system.getSorts().getSorts().get(0).getName().getValue(), Is.is("N"));
        assertThat(system.getSorts().getSorts().get(1).getName().getValue(), Is.is("Q"));
    }

    @Test
    public void testOperationWithSingleDomainAndCodomain() throws Exception {
        final String input = "operations { "
                + "s:N->N; "
                + "}";
        final SystemDeclaration system = parse(input);
        assertNotNull(system);
        assertNotNull(system.getOperations());
        assertThat(system.getOperations().getOperations().size(), Is.is(1));
        assertThat(system.getOperations().getOperations().get(0), op("s",single("N"),single("N")));
    }

    @Test
    public void testConstOperation() throws Exception {
        final String input = "operations { "
                + "0:->N; "
                + "}";
        final SystemDeclaration system = parse(input);
        assertNotNull(system);
        assertNotNull(system.getOperations());
        assertThat(system.getOperations().getOperations().size(), Is.is(1));
        assertThat(system.getOperations().getOperations().get(0), op("0",constant(),single("N")));
    }

    @Test
    public void testProductOperation() throws Exception {
        final String input = "operations { "
                + "swap:N,N->N,N; "
                + "}";
        final SystemDeclaration system = parse(input);
        assertNotNull(system);
        assertNotNull(system.getOperations());
        assertThat(system.getOperations().getOperations().size(), Is.is(1));
        assertThat(system.getOperations().getOperations().get(0), op("swap",product("N","N"),product("N","N")));
    }


    public static SystemDeclaration parse(final String input) throws ParserException, ScannerException {
        final Scanner scanner = new Scanner();
        final Parser parser = new Parser();
        return (SystemDeclaration) parser.parse(scanner.scan(input));
    }

    public static Matcher<OperationDeclaration> op(final String name, final Matcher<SortsReferences> domain, final Matcher<SortsReferences> codomain) {
        return new BaseMatcher<OperationDeclaration>() {

            public boolean matches(final Object item) {
                if (item instanceof OperationDeclaration) {
                    final OperationDeclaration op = (OperationDeclaration) item;
                    return Is.is(name).matches(op.getOperationName().getValue()) && domain.matches(op.getDomain()) && codomain.matches(op.getCodomain());
                }
                return false;
            }

            public void describeTo(final Description description) {
                description.appendText("Name is ").appendText(name);
                description.appendText("domain is ").appendDescriptionOf(domain);
                description.appendText("codomain is ").appendDescriptionOf(codomain);
            }
        };
    }

    public static Matcher<SortsReferences> constant() {
        return new BaseMatcher<SortsReferences>() {

            public boolean matches(final Object item) {
                return item == null;
            }

            public void describeTo(final Description description) {
                description.appendText("is empty");
            }
        };
    }

    public static Matcher<SortsReferences> single(final String sortName) {
        return new BaseMatcher<SortsReferences>() {

            public boolean matches(final Object item) {
                if (item instanceof SingleSortReference) {
                    return Is.is(sortName).matches(((SingleSortReference)item).getSortName().getValue());
                }
                return false;
            }

            public void describeTo(final Description description) {
                description.appendText("Sort name:");
                description.appendValue(sortName);
            }
        };
    }

    public static Matcher<SortsReferences> product(final String ...sorts) {
        final List<Matcher<SortsReferences>> matchers = Lists.newArrayList();
        for (final String sort : sorts) {
            matchers.add(single(sort));
        }

        return new BaseMatcher<SortsReferences>() {

            public boolean matches(final Object item) {
                if (item instanceof ProductSortReference) {
                    final Iterator<SingleSortReference> refIterator = ((ProductSortReference) item).getSortReferences().iterator();
                    final Iterator<Matcher<SortsReferences>> matcherIterator = matchers.iterator();
                    while (matcherIterator.hasNext() && refIterator.hasNext()) {
                        final SingleSortReference sortReference = refIterator.next();
                        final Matcher<SortsReferences> matcher = matcherIterator.next();
                        if (!matcher.matches(sortReference)) {
                            return false;
                        }
                    }
                    return !refIterator.hasNext() && !matcherIterator.hasNext();
                }
                return false;
            }

            public void describeTo(final Description description) {
                description.appendList("Is product of", ", ", "", matchers);
            }

        };
    }

}
